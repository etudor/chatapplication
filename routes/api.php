<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'users', 'middleware' => 'api'], function () {
    Route::post('/register', '\App\Http\Controllers\Auth\RegisterController@register');
    Route::post('/login', '\App\Http\Controllers\Auth\LoginController@login');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/me', '\App\Http\Controllers\Auth\UserController@show');
        Route::put('/me', '\App\Http\Controllers\Auth\UserController@update');
    });
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('chats', '\App\Http\Controllers\ChatController@create');
    Route::get('chats', '\App\Http\Controllers\ChatController@list');

    Route::post('chats/{chat}/messages', '\App\Http\Controllers\MessagesController@create');
    Route::get('chats/{chat}/messages', '\App\Http\Controllers\MessagesController@list');
});
