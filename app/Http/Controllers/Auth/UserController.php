<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\Repository\UserRepository;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return []
     */
    public function show()
    {
        return $this->apiSuccess($this->user());
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), User::$rules);

        if ($validator->passes()) {
            $user = $this->userRepository->updateUser(Auth::user(), $request->all());

            return $this->apiSuccess($user);
        }

        return $this->apiError($validator->errors());
    }
}
