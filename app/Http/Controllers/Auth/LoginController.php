<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\Repository\UserRepository;
use App\Model\User;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var BcryptHasher
     */
    protected $bcryptHasher;

    /**
     * Create a new controller instance.
     *
     * @param UserRepository $userRepository
     * @param BcryptHasher $bcryptHasher
     */
    public function __construct(UserRepository $userRepository, BcryptHasher $bcryptHasher)
    {
        $this->userRepository = $userRepository;
        $this->bcryptHasher = $bcryptHasher;
    }

    /**
     * Handle a login request to the application.
     *
     * @param  Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required',
            'password' => 'required',
        ]);

        if ($validator->passes()) {
            $user = $this->userRepository->findOneByEmail(
                $request->get('email')
            );

            if ($user && $this->bcryptHasher->check($request->get('password'), $user->password)) {
                return $this->apiSuccess($user);
            }
        }

        return $this->apiError($validator->errors());
    }
}
