<?php

namespace App\Http\Controllers\Auth;

use App\Model\Factory\UserFactory;
use App\Model\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @var Auth
     */
    private $auth;

    /**
     * Create a new controller instance.
     */
    public function __construct(UserFactory $userFactory, Auth $auth)
    {
        $this->middleware('api');

        $this->userFactory = $userFactory;
        $this->auth = $auth;
    }

    /**
     * @param Request $request
     *
     * @return array []
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), User::$rules);

        if ($validator->passes()) {
            $user = $this->userFactory->createFromInputData($request->all());

            Auth::guard()->login($user);

            return $this->apiSuccess($user);
        }

        return $this->apiError($validator->errors());
    }
}
