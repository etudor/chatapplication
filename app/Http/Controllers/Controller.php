<?php

namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @return User|null
     */
    public function user()
    {
        return Auth::user();
    }

    /**
     * @param $pagination
     * @param int $limit
     *
     * @return array
     */
    protected function getPaginationInfo($pagination, $limit)
    {
        return [
            'page_count'        => $pagination->lastPage(),
            'current_page'      => $pagination->currentPage(),
            'has_next_page'     => $pagination->nextPageUrl() !== null,
            'has_previous_page' => $pagination->previousPageUrl() !== null,
            'count'             => $pagination->total(),
            'limit'             => $limit,
        ];
    }

    /**
     * @param [] $errors
     *
     * @return []
     */
    protected function apiError($errors)
    {
        return [
            'success' => false,
            'errors' => $errors
        ];
    }

    /**
     * @param $data
     * @return array []
     */
    protected function apiSuccess($data)
    {
        return [
            'success' => true,
            'data' => $data
        ];
    }
}
