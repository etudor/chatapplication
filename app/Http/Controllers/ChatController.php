<?php

namespace App\Http\Controllers;


use App\Model\Chat;
use App\Model\Factory\ChatFactory;
use App\Model\Repository\ChatRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    /**
     * @var ChatFactory
     */
    private $chatFactory;
    /**
     * @var ChatRepository
     */
    private $chatRepository;

    public function __construct(ChatFactory $chatFactory, ChatRepository $chatRepository)
    {
        $this->chatFactory = $chatFactory;
        $this->chatRepository = $chatRepository;
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), Chat::$rules);

        if ($validator->passes()) {
            $chat = $this->chatFactory->createFromRequestForUser($this->user(), $request->all());

            return $this->apiSuccess($chat);
        }

        return $this->apiError($validator->errors());
    }

    /**
     * @param Request $request
     *
     * @return array []
     */
    public function list(Request $request)
    {
        $limit = (int)$request->get('limit', null);
        $query = $request->get('q');

        $pagination = $this->chatRepository->search($query, $limit);

        return [
            'success' => true,
            'data' => $pagination->all(),
            'pagination' => $this->getPaginationInfo($pagination, $limit)
        ];
    }
}
