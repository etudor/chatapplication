<?php

namespace App\Http\Controllers;

use App\Model\Chat;
use App\Model\Factory\MessageFactory;
use App\Model\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MessagesController extends Controller
{
    /**
     * @var MessageFactory
     */
    private $messageFactory;

    public function __construct(MessageFactory $messageFactory)
    {
        $this->messageFactory = $messageFactory;
    }

    /**
     * @param Request $request
     * @param Chat $chat
     * @return []
     */
    public function create(Request $request, Chat $chat)
    {
        $validator = Validator::make($request->all(), Message::$rules);

        if ($validator->passes()) {

            $message = $this->messageFactory->createForChatAndUserFromRequest(
                $this->user(),
                $chat,
                $request->all()
            );

            return $this->apiSuccess($message);
        }

        return $this->apiError($validator->errors());
    }

    /**
     * @param Request $request
     * @param Chat $chat
     * @return array []
     */
    public function list(Request $request, Chat $chat)
    {
        $limit = (int)$request->get('limit', null);

        $pagination = $chat->messages()->paginate($limit);

        $data = [
            'messages'   => $pagination->all(),
            'pagination' => $this->getPaginationInfo($pagination, $limit),
        ];

        return $this->apiSuccess($data);
    }
}
