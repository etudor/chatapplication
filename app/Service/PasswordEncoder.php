<?php

namespace App\Service;

class PasswordEncoder
{
    /**
     * @param string $password
     *
     * @return string
     */
    public function encode(string $password)
    {
        return bcrypt($password);
    }
}
