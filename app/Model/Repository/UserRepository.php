<?php

namespace App\Model\Repository;

use App\Model\User;
use App\Service\PasswordEncoder;

class UserRepository
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var PasswordEncoder
     */
    private $passwordEncoder;

    /**
     * @param User            $user
     * @param PasswordEncoder $passwordEncoder
     */
    public function __construct(User $user, PasswordEncoder $passwordEncoder)
    {
        $this->user = $user;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param $email
     *
     * @return User|null
     */
    public function findOneByEmail($email)
    {
        return $this->user
            ->where('email', $email)
            ->first();
    }

    public function updateUser(User $user, array $data)
    {
        $user->email = $data['email'];
        $user->name = $data['name'];
        $user->password = $this->passwordEncoder->encode($data['password']);
        $user->save();

        return $user;
    }

    /**
     * @param string $email
     */
    public function removeByEmail($email)
    {
        $user = $this->user->where('email', $email);

        if ($user) {
            $user->delete();
        }
    }
}
