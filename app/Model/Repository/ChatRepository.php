<?php

namespace App\Model\Repository;

use App\Model\Chat;

class ChatRepository
{
    /**
     * @var Chat
     */
    private $chat;

    public function __construct(Chat $chat)
    {
        $this->chat = $chat;
    }
    
    /**
     * @param $query
     *
     * @return mixed
     */
    public function search($query, $limit)
    {
        return $this->chat->where('name', 'LIKE', '%' . $query . '%')->paginate($limit);
    }
}
