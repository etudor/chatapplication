<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public static $rules = [
        'name'     => 'required|max:255',
        'email'    => 'required|email|max:255|unique:users',
        'password' => 'required|min:6',
        'confirm'  => 'same:password'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'updated_at', 'created_at'
    ];

    /**
     * @return []
     */
    public function getShortInfo()
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
