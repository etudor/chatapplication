<?php

namespace App\Model\Factory;

use App\Model\Chat;
use App\Model\Message;
use App\Model\User;

class MessageFactory
{
    /**
     * @return Message
     */
    public function create()
    {
        return new Message();
    }

    /**
     * @param User $user
     * @param Chat $chat
     * @param array $data
     *
     * @return Message
     */
    public function createForChatAndUserFromRequest(User $user, Chat $chat, array $data)
    {
        $message = $this->create();
        $message->user()->associate($user);
        $message->chat()->associate($chat);
        $message->message = $data['message'];
        $message->save();

        return $message;
    }
}
