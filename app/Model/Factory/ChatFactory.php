<?php

namespace App\Model\Factory;

use App\Model\Chat;
use App\Model\User;

class ChatFactory
{
    /**
     * @return Chat
     */
    public function create()
    {
        return new Chat();
    }

    public function createFromRequestForUser(User $user, array $data)
    {
        $chat = $this->create();
        $chat->name = $data['name'];
        $chat->user()->associate($user);
        $chat->save();

        return $chat;
    }
}
