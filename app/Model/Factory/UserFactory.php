<?php

namespace App\Model\Factory;

use App\Model\User;
use App\Service\PasswordEncoder;

class UserFactory
{
    /**
     * @var PasswordEncoder
     */
    private $passwordEncoder;

    public function __construct(PasswordEncoder $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param array $data
     *
     * @return User
     */
    public function createFromInputData(array $data)
    {
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = $this->passwordEncoder->encode($data['password']);
        $user->api_token = str_random(60);

        $user->save();

        return $user;
    }

}
