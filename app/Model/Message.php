<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public static $rules = [
        'message' => 'required'
    ];

    protected $appends = ['user_data'];

    protected $hidden = [
        'user', 'chat', 'updated_at'
    ];

    /**
     * @return []
     */
    public function getUserDataAttribute()
    {
        return $this->user->getShortInfo();
    }

    public function chat()
    {
        return $this->belongsTo('App\Model\Chat', 'chat_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }
}
