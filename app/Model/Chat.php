<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Chat extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'user'
    ];

    public static $rules = [
        'name' => 'required'
    ];

    protected $appends = ['user_data'];

    /**
     * @return []
     */
    public function getUserDataAttribute()
    {
        return $this->user->getShortInfo();
    }
    
    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }

    public function messages()
    {
        return $this->hasMany('App\Model\Message');
    }
}
