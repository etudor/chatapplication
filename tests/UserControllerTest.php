<?php

use App\Model\Chat;
use App\Model\Factory\UserFactory;
use App\Model\Repository\ChatRepository;
use App\Model\Repository\UserRepository;
use App\Model\User;
use Illuminate\Http\Request;

class UserControllerTest extends TestCase
{
    public static $name = 'tudorel';
    public static $email = 'tudorel@yopmail.com';
    public static $password = 'password';

    /**
     * @var User
     */
    protected $user;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var UserFactory
     */
    protected $userFactory;

    /**
     * @var ChatRepository
     */
    protected $chatRepository;

    /**
     * @var Chat
     */
    protected $chat;

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->app->make(User::class);
        $this->userRepository = $this->app->make(UserRepository::class);
        $this->userFactory = $this->app->make(UserFactory::class);
        $this->chatRepository = $this->app->make(ChatRepository::class);
        $this->chat = $this->app->make(Chat::class);
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $this->userFactory->createFromInputData([
            'name'     => self::$name,
            'email'    => self::$email,
            'password' => self::$password,
            'confirm'  => self::$password,
        ]);

        $response = $this->call(Request::METHOD_POST, '/api/users/login', [
            'email'    => self::$email,
            'password' => self::$password,
        ]);

        $data = json_decode($response->getContent(), true);

        $this->assertTrue($data['success']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('api_token', $data['data']);
    }

    public function testRegister()
    {
        $response = $this->call(Request::METHOD_POST, '/api/users/register', [
            'name'     => self::$name,
            'email'    => self::$email,
            'password' => self::$password,
            'confirm'  => self::$password,
        ]);

        $data = json_decode($response->getContent(), true);

        $this->assertTrue($data['success']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('name', $data['data']);
        $this->assertArrayHasKey('email', $data['data']);
        $this->assertArrayHasKey('id', $data['data']);
        $this->assertArrayHasKey('api_token', $data['data']);
    }

    public function testShowUser()
    {
        $response = $this->loggedInCall(Request::METHOD_GET, 'api/users/me');

        $data = json_decode($response->getContent(), true);

        $this->assertTrue($data['success']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('name', $data['data']);
        $this->assertArrayHasKey('email', $data['data']);
        $this->assertArrayHasKey('api_token', $data['data']);
    }

    public function testUpdateUser()
    {
        $newName = 'tudorel2';
        $newEmail = 'tudorel2@test.com';

        $this->userRepository->removeByEmail($newEmail);

        $inputData = [
            'name'     => $newName,
            'email'    => $newEmail,
            'password' => self::$password,
            'confirm'  => self::$password,
        ];

        $response = $this->loggedInCall(Request::METHOD_PUT, 'api/users/me', $inputData);

        $data = json_decode($response->getContent(), true);

        $this->assertTrue($data['success']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('name', $data['data']);
        $this->assertArrayHasKey('email', $data['data']);
        $this->assertArrayHasKey('api_token', $data['data']);

        $this->assertEquals($newName, $data['data']['name']);
        $this->assertEquals($newEmail, $data['data']['email']);
    }

    public function testCreateChat()
    {
        $chatName = 'new Chat';
        $inputData = [
            'name' => $chatName
        ];

        $response = $this->loggedInCall(Request::METHOD_POST, 'api/chats', $inputData);

        $data = json_decode($response->getContent(), true);

        $this->assertTrue($data['success']);
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals($chatName, $data['data']['name']);
        $this->assertArrayHasKey('user_id', $data['data']);
        $this->assertArrayHasKey('user_data', $data['data']);
        $this->assertEquals(self::$name, $data['data']['user_data']['name']);
    }

    public function testCreateMessage()
    {
        $messageName = 'new Message';
        $inputData = [
            'message' => $messageName
        ];

        $chat = $this->chat->first();
        $response = $this->loggedInCall(Request::METHOD_POST, 'api/chats/' . $chat->id . '/messages', $inputData);

        $data = json_decode($response->getContent(), true);

        $this->assertTrue($data['success']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user_id', $data['data']);
        $this->assertArrayHasKey('chat_id', $data['data']);
        $this->assertArrayHasKey('user_data', $data['data']);
        $this->assertEquals(self::$name, $data['data']['user_data']['name']);
        $this->assertEquals($messageName, $data['data']['message']);
    }

    public function testListMessages()
    {
        $chat = $this->chat->first();
        $response = $this->loggedInCall(Request::METHOD_GET, 'api/chats/' . $chat->id . '/messages');

        $data = json_decode($response->getContent(), true);

        $this->assertTrue($data['success']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('messages', $data['data']);
        $this->assertArrayHasKey('pagination', $data['data']);
    }

    public function testListChats()
    {
        $chat = $this->chat->first();
        $response = $this->loggedInCall(Request::METHOD_GET, 'api/chats');

        $data = json_decode($response->getContent(), true);

        $this->assertTrue($data['success']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('pagination', $data);
    }

    public function tearDown()
    {
        $this->userRepository->removeByEmail(self::$email);
    }

    private function createUser()
    {
        return $this->userFactory->createFromInputData([
            'name'     => self::$name,
            'email'    => self::$email,
            'password' => self::$password,
            'confirm'  => self::$password,
        ]);
    }

    /**
     * @param $method
     * @param $uri
     *
     * @return \Illuminate\Http\Response
     */
    protected function loggedInCall($method, $uri, $data = [])
    {
        $user = $this->createUser();

        return $this->call($method, $uri, $data, [], [], [
            'HTTP_CONTENT_TYPE'  => 'application/json',
            'HTTP_ACCEPT'        => 'application/json',
            'HTTP_Authorization' => 'Bearer ' . $user->api_token,
        ]);
    }
}
