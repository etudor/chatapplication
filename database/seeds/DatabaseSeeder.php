<?php

use App\Model\Factory\UserFactory;
use App\Model\Repository\UserRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * @var UserFactory
     */
    private $userFactory;

    public function __construct(UserFactory $userFactory)
    {
        $this->userFactory = $userFactory;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $testUser = $this->userFactory->createFromInputData([
            'name' => 'Tudorel',
            'email' => 'tudorel@yopmail.com',
            'password' => 'password'
        ]);
    }
}
